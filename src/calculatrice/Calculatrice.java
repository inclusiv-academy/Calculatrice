/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculatrice;

/**
 *
 * @author mamia
 */
public class Calculatrice {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here

    }

    public static double effectuerAddition(double nb1, double nb2) {
        return nb1 + nb2;
    }

    public static double effectuerSoustraction(double nb1, double nb2) {
        return nb1 - nb2;
    }

    public static double EffectuerMultplication(double nb1, double nb2) {
        return nb1 * nb2;
    }

    public static double effectuerDivision(double dividende, double diviseur) {
        if (diviseur == 0) {
            throw new IllegalArgumentException("Le diviseur ne peut pas être zéro.");
        }
        return dividende / diviseur;
    }

    public static double calculerMoyenne(double[] liste) {
        if (liste.length == 0) {
            throw new IllegalArgumentException("La liste ne peut pas être vide.");
        }
        double somme = 0;
        for (double nombre : liste) {
            somme += nombre;
        }
        double moyenne = somme / liste.length;
        return moyenne;
    }
}

